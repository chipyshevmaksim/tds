// Fill out your copyright notice in the Description page of Project Settings.


#include "TDSHealthCharacterComponent.h"

void UTDSHealthCharacterComponent::ChangeHealthValue(float ChangeValue)
{
	float CurrentDamage = CoefDamage * ChangeValue;

	if (Shield > 0.0f && ChangeValue < 0.0f)//���� �� ������� � 0.1 && ���������� ��� ��� ����(������������� �����) � ��� � ���, ������� �������� ���������
	{
		//Shield -= CurrentDamage;������� �����������
		ChangeShieldValue(ChangeValue);
		if (Shield < 0.0f)
		{
			//FX
		}

	}
	else
	{
		Super::ChangeHealthValue(ChangeValue);
	}
}

float UTDSHealthCharacterComponent::GetCurrentShield()
{
	return Shield;
}

void UTDSHealthCharacterComponent::ChangeShieldValue(float ChangeValue)
{
	Shield += ChangeValue;

	if (Shield > 100.0f)
	{
		Shield = 100.0f;
	}
	else
	{
		if (Shield < 0.0f)
		{
			Shield = 0.0f;
		}
	}

	if (GetWorld())
	{
		GetWorld()->GetTimerManager().SetTimer(TimerHandle_CoolDownShieldTimer, this, &UTDSHealthCharacterComponent::CoolDownShieldEnd, CoolDownShieldRecoveryTime, false);

		GetWorld()->GetTimerManager().ClearTimer(TimerHandle_ShieldRecoveryRateTimer);
	}	

	OnShieldChange.Broadcast(Shield, ChangeValue);
}

void UTDSHealthCharacterComponent::CoolDownShieldEnd()
{
	if (GetWorld())
	{
		GetWorld()->GetTimerManager().SetTimer(TimerHandle_ShieldRecoveryRateTimer, this, &UTDSHealthCharacterComponent::RecoveryShield, ShieldRecoveryRate, true);
	}
}

void UTDSHealthCharacterComponent::RecoveryShield()
{
	float tmp = Shield;
	tmp = tmp + ShieldRecoveryValue;
	if (tmp > 100.0f)
	{
		Shield = 100.0f;
		if (GetWorld())
		{
			GetWorld()->GetTimerManager().ClearTimer(TimerHandle_ShieldRecoveryRateTimer);
		}
	}
	else
	{
		Shield = tmp;
	}

	OnShieldChange.Broadcast(Shield, ShieldRecoveryValue);
}
